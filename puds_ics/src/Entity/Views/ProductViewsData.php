<?php
namespace Drupal\puds_ics\Entity\Views;

use Drupal\views\EntityViewsDataInterface;

/**
 * Class ProductViewsData
 * @package Drupal\puds_ics\Entity\Views
 */
class ProductViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   * @see https://api.drupal.org/api/views/views.api.php/function/hook_views_data/7.x-3.x
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $data['ics_product'] = [
      'table' => [
        'group' => 'ICS',
        'base' => [
          'field' => 'id',
          'title' => 'ICS product',
          'help' => 'Contains ICS products',
        ],
        'id' => [
          'title' => t('ID'),
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_numeric',
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
        ],
        'name' => [
          'title' => t('Name'),
          'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_string',
          ),
          'argument' => array(
            'handler' => 'views_handler_argument_string',
          ),
        ],
      ]
    ];
    return $data;
  }

  public function getViewsTableForEntityType(EntityTypeInterface $entity_type) {
    return 'ics_products';
  }
}