<?php
namespace Drupal\puds_ics\Entity\Views;

use Drupal\views\EntityViewsDataInterface;

/**
 * Class TermViewsData
 * @package Drupal\puds_ics\Entity\Views
 */
class TermViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   * @see https://api.drupal.org/api/views/views.api.php/function/hook_views_data/7.x-3.x
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $data['ics_history'] = [
      'table' => [
        'group' => 'ICS',
        'base' => [
          'field' => 'id',
          'title' => 'ICS history',
          'help' => 'Contains ICS history',
        ],
        'join' => [
          'ics_products' => [
            'left_field' => 'id',
            'field' => 'productid'
          ]
        ],
        'id' => [
          'title' => t('ID'),
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_numeric',
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
        ],
        'minprize' => [
          'title' => t('Minimum prize'),
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_numeric',
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
        ],
        'maxprize' => [
          'title' => t('Maximum prize'),
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_numeric',
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
        ],
        'year' => [
          'title' => t('Year'),
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_numeric',
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),

        ],
        'week' => [
          'title' => t('Week'),
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_numeric',
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
        ],
        'productid' => [
          'title' => t('Product'),
          'relationship' => array(
            'base' => 'ics_product',
            'base field' => 'id',
            'handler' => 'views_handler_relationship',
            'label' => t('Related product'),
            'title' => t('Related product title'),
            'help' => t('Related product help'),
          ),
        ],
      ]
    ];
    return $data;
  }

  public function getViewsTableForEntityType(EntityTypeInterface $entity_type) {
    return 'ics_history';
  }
}