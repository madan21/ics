<?php
/**
 * @file
 * Contains \Drupal\content_entity_example\Entity\ContentEntityExample.
 */

namespace Drupal\puds_ics\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup puds_ics
 *
 *
 * @ContentEntityType(
 *   id = "ics_product",
 *   label = @Translation("ICS Product"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\puds_ics\Entity\Controller\TermListBuilder",
 *     "access" = "Drupal\puds_ics\IcsAccessControlHandler",
 *   },
 *   base_table = "ics_products",
 *   admin_permission = "administer ics entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "name" = "name",
 *     "shortname" = "shortname",
 *     "foldername" = "foldername",
 *     "description" = "description"
 *   },
 *   links = {
 *     "canonical" = "/admin/ics/{ics_product}",
 *     "collection" = "/admin/ics/list"
 *   }
 * )
 */
class Product extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    // Default author to current user.
    /*$values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );*/
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Term entity.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of product.'))
      ->setRequired(true);

    $fields['shortname'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Short name'))
      ->setDescription(t('The short name of product.'))
      ->setRequired(true);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setRequired(true);

    return $fields;
  }
}
