<?php
/**
 * @file
 * Contains \Drupal\content_entity_example\Entity\ContentEntityExample.
 */

namespace Drupal\puds_ics\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup puds_ics
 *
 *
 * @ContentEntityType(
 *   id = "ics_term",
 *   label = @Translation("ICS Term"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\puds_ics\Entity\Controller\TermListBuilder",
 *     "form" = {
 *       "add" = "Drupal\puds_ics\Form\TermForm",
 *       "edit" = "Drupal\puds_ics\Form\TermForm",
 *       "delete" = "Drupal\puds_ics\Form\TermDeleteForm",
 *     },
 *     "access" = "Drupal\puds_ics\IcsAccessControlHandler",
 *   },
 *   base_table = "ics_history",
 *   admin_permission = "administer ics entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "minprize" = "minprize",
 *     "maxprize" = "maxprize",
 *     "week" = "week",
 *     "year" = "year",
 *     "productid" = "productid",
 *   },
 *   links = {
 *     "canonical" = "/admin/ics/{ics_term}",
 *     "edit-form" = "/admin/ics/{ics_term}/edit",
 *     "delete-form" = "/admin/ics/{ics_term}/delete",
 *     "collection" = "/admin/ics/list"
 *   }
 * )
 */
class Term extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Term entity.'))
      ->setReadOnly(TRUE);

    $fields['minprize'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Min prize'))
      ->setDescription(t('The minimum prize of product.'))
      ->setRequired(true);

    $fields['maxprize'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Max prize'))
      ->setDescription(t('The maximum prize of product.'))
      ->setRequired(true);

    $fields['week'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Week'))
      ->setDescription(t('Week'))
      ->setRequired(true);

    $fields['year'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Year'))
      ->setDescription(t('Year'))
      ->setRequired(true);

    $fields['productid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product'))
      ->setDescription(t('Product ID'))
      ->setSetting('target_type', 'ics_product')
      ->setSetting('handler', 'default');

    return $fields;
  }
}
