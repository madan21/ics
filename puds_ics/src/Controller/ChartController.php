<?php

namespace Drupal\puds_ics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\puds_ics\Helper\Helper;
use Drupal\puds_ics\Model\Product;
use Zend\Diactoros\Response\JsonResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Defines ChartController class.
 */
class ChartController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @param int $id
   * @return array
   *   Return markup array.
   */
  public function content($id) {
    $product = new Product($id);
    return  [
      '#theme' => 'chart_svg',
      '#id' => $id,
      '#name' => $product->getAttribute('name'),
      '#description' => $product->getProductDetail()['description']
    ];
  }

  /**
   * @param int $id
   * @param string $startdate
   * @param string $enddate
   * @param string $json default true
   * @return JsonResponse
   */
  public function get($id, $startdate, $enddate, $json = true){

    $product = new Product($id);
    $response['status'] = 'error';
    $startyear = '';
    $startweek = '';
    $endyear = '';
    $endweek = '';
    //set current week
    if (isset($_GET['showAll']) && $_GET['showAll']) { //if get parameter with showAll true
      [$currentweek, $currentyear] = Helper::getWeekYear();
    }else{
      [$currentweek, $currentyear] = Helper::getWeekYear(-1);
    }
    //for guests always current week - last 12 weeks
    if (\Drupal::currentUser()->isAnonymous()) {
      $endweek = $currentweek;
      $endyear = $currentyear;
      [$startweek, $startyear] = Helper::getWeekYear(-12);
    //for logged in users
    } else {
      //set start week/year
      if ($startdate) {
        $startyear = date('Y', strtotime($startdate . ' 00:00:01'));
        $startweek = date('W', strtotime($startdate));
      }
      if (!$startyear) {
        $startyear = Helper::ICS_START_YEAR;
        $startweek = Helper::ICS_START_WEEK;
      }
      //set end week/year
      if ($enddate) {
        $endyear = date('Y', strtotime($enddate));
        $endweek = date('W', strtotime($enddate));
      }
      if (!$endyear || ($endweek > $currentweek && $endyear >= $currentyear)) {
        $endweek = $currentweek;
        $endyear = $currentyear;
      }
      if ($startyear == $endyear && $startweek > $endweek) {
        $startyear--;
      }
    }

    //get data
    $data = $product->getHistory($startweek, $startyear, $endweek, $endyear);
    $response = [
      'status' => 'success',
      'name' => $product->getAttribute('name'),
      'shortname' => $product->getAttribute('shortname'),
      'min' => '',
      'max' => '',
      'week' => '',
      'year' => '',
    ];
    if($data){
      $last = $data[count($data)-1];
      $previous = $data[count($data)-2];
      $response['data'] = $data;
      $response['week'] = $last['w'];
      $response['year'] = $last['y'];
      $response['min'] = $last['min'];
      $response['max'] = $last['max'];
      if(!\Drupal::currentUser()->isAnonymous()) {
        //get data for previous week
        $response['data_previous_week'] = $product->getHistory($previous['w'], $previous['y'], $previous['w'], $previous['y']);
        if($response['data_previous_week'][0]) {
          $response['data_previous_week'] = $response['data_previous_week'][0];
          $response['data_previous_week']['change_min'] = $last['min'] - $response['data_previous_week']['min'];
          $response['data_previous_week']['change_min'] = ($response['data_previous_week']['change_min'] > 0 ? '+' : '') . $response['data_previous_week']['change_min'];
          $response['data_previous_week']['change_max'] = $last['max'] - $response['data_previous_week']['max'];
          $response['data_previous_week']['change_max'] = ($response['data_previous_week']['change_max'] > 0 ? '+' : '') . $response['data_previous_week']['change_max'];
          $response['data_previous_week']['percent_min'] = number_format($response['data_previous_week']['change_min'] / $response['data_previous_week']['min'] * 100, 2);
          $response['data_previous_week']['percent_min'] = ($response['data_previous_week']['percent_min'] > 0 ? '+' : '') . $response['data_previous_week']['percent_min'];
          $response['data_previous_week']['percent_max'] = number_format($response['data_previous_week']['change_max'] / $response['data_previous_week']['max'] * 100, 2);
          $response['data_previous_week']['percent_max'] = ($response['data_previous_week']['percent_max'] > 0 ? '+' : '') . $response['data_previous_week']['percent_max'];
        }
        //get data for previous year
        $response['data_previous_year'] = $product->getHistory($endweek, $endyear-1, $endweek, $endyear-1);
        if($response['data_previous_year'][0]) {
          $response['data_previous_year'] = $response['data_previous_year'][0];
          $response['data_previous_year']['change_min'] = $last['min'] - $response['data_previous_year']['min'];
          $response['data_previous_year']['change_min'] = ($response['data_previous_year']['change_min'] > 0 ? '+' : '') . $response['data_previous_year']['change_min'];
          $response['data_previous_year']['change_max'] = $last['max'] - $response['data_previous_year']['max'];
          $response['data_previous_year']['change_max'] = ($response['data_previous_year']['change_max'] > 0 ? '+' : '') . $response['data_previous_year']['change_max'];
          $response['data_previous_year']['percent_min'] = number_format($response['data_previous_year']['change_min'] / $response['data_previous_year']['min'] * 100, 2);
          $response['data_previous_year']['percent_min'] = ($response['data_previous_year']['percent_min'] > 0 ? '+' : '') . $response['data_previous_year']['percent_min'];
          $response['data_previous_year']['percent_max'] = number_format($response['data_previous_year']['change_max'] / $response['data_previous_year']['max'] * 100, 2);
          $response['data_previous_year']['percent_max'] = ($response['data_previous_year']['percent_max'] > 0 ? '+' : '') . $response['data_previous_year']['percent_max'];
        }
      }
    }
    return $json?new JsonResponse($response):$response;
  }

  /**
   * //@TODO create service using symfony upload method.
   * @return JsonResponse
   */
  public function uploadImage()
  {
    $response = [
      'status' => 'error'
    ];
    $request = \Drupal::request();
    $is_ajax = $request->isXmlHttpRequest();
    if($is_ajax){
      //@TODO create upload service
      $uploaddir = 'sites/default/files/cache_ics_images/';
      //check if file already exists
      if(file_exists($uploaddir.$_FILES['documentFile']['name'])){
        $response['status'] = 'success';
        $response['message'] = 'Files already exists';
      }else{
        $uploadfile = $uploaddir . basename($_FILES['documentFile']['name']);
        if (move_uploaded_file($_FILES['documentFile']['tmp_name'], $uploadfile)) {
          $response['status'] = 'success';
          $response['message'] = 'Files successfully uploaded';
        } else {
          $response['message'] = 'Cannot upload files';
        }
      }
      return new JsonResponse($response);
    }
  }
}