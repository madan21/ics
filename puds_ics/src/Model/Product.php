<?php
namespace Drupal\puds_ics\Model;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Select;
use http\Exception\InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * Class Product
 * @package Drupal\puds_ics\Model
 */

class Product {

  /**
   * @var $id
   */
  protected $id;

  /**
   * @var array
   */
  protected $data = [];

  /**
   * @var Connection
   */
  protected $connection;

  /**
   * Product constructor.
   * @param int $id
   */
  public function __construct($id) {
    $this->id = $id;
    $this->connection = \Drupal::service('database'); /** @var Connection $db */
    $query = $this->connection->select('ics_products', 'i')->fields('i')->where('id=:id',[':id' => $this->id]);
    $product = $query->execute()->fetch();
    if(empty($product)) {
      throw new InvalidParameterException();
    } else {
      foreach($product as $key=>$value) {
        $this->data[$key] = $value;
      }
    }
  }

  /**
   * @param string $attribute
   * @return string
   */
  public function getAttribute($attribute) {
    if(!isset($this->data[$attribute])) {
      throw new \InvalidArgumentException();
    }
    return $this->data[$attribute];
  }

  public function getHistory($startweek, $startyear, $endweek, $endyear) {

    $connection = \Drupal::service('database');
    $sql = 'SELECT h.id,p.name,p.shortname,h.minprize,h.maxprize,h.week,h.year,h.productid ' .
      'FROM {ics_products} p INNER JOIN {ics_history} h ' .
      'ON p.id = h.productid ';

    $params = [];
    $params[':id'] = $this->id;
    $params[':startyear'] = $startyear;
    $params[':startweek'] = $startweek;
    $params[':endweek'] = $endweek;

    if($startyear==$endyear) {
      $where = 'WHERE h.year=:startyear AND h.week BETWEEN :startweek AND :endweek AND h.productid=:id ';
    } else {
      $where = 'WHERE h.year BETWEEN :startyear AND :endyear AND h.productid=:id AND ' .
        'CASE '.
        'WHEN h.year = :startyear THEN h.week >= :startweek '.
        'WHEN h.year = :endyear THEN h.week <= :endweek '.
        'ELSE h.week < 54 END ';
      $params[':endyear'] = $endyear;
    }

    $orderby = 'ORDER BY h.year,h.week';
    $query = $connection->query($sql.$where.$orderby, $params);
    $data = [];
    foreach($query->fetchAll() as $row) {
      $d = [];
      $d['min'] = $row->minprize;
      $d['max'] = $row->maxprize;
      $d['w'] = $row->week;
      $d['y'] = $row->year;
      $data[] = $d;
    }
    return $data;
  }

  /**
   * Return product information from ics_products table.
   * @return array
   */
  public function getProductDetail() {
    return $this->data;
  }


}