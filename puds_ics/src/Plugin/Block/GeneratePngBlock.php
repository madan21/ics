<?php
/**
 * @file
 * Contains \Drupal\puds_ics\Plugin\Block\ChartBlock.
 */
namespace Drupal\puds_ics\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\puds_ics\Helper\Helper;

/**
 * Provides a 'charts' block.
 *
 * @Block(
 *   id = "generate_png_block",
 *   admin_label = @Translation("admin ICS charts block"),
 *   category = @Translation("PUDS custom blocks")
 * )
 */
class GeneratePngBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return  [
      '#theme' => 'generate_png_block',
      '#products' => Helper::getProducts()
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }
}