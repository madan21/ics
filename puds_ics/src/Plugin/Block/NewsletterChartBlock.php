<?php
/**
 * @file
 * Contains \Drupal\puds_ics\Plugin\Block\ChartBlock.
 */
namespace Drupal\puds_ics\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\puds_ics\Helper\Helper;
use Drupal\puds_ics\Controller\ChartController;

/**
 * Provides a 'charts' block.
 *
 * @Block(
 *   id = "newsletter_chart_block",
 *   admin_label = @Translation("Newsletter Chart Block"),
 *   category = @Translation("PUDS custom blocks")
 * )
 */
class NewsletterChartBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $products = Helper::getProducts();
    $details = [];
    $chartController = new ChartController();
    foreach($products as $key => $val)
    {
      //start and end date is today to get less query data response
      $details[] = $chartController->get($key,date('Y-m-d'),date('Y-m-d'),false);
    }
    return  [
      '#theme' => 'newsletter_chart_block',
      '#productsDetail' => $details
    ];
  }

  /**
   * {@inheritdoc}
   */
//  protected function blockAccess(AccountInterface $account) {
//    return AccessResult::allowedIfHasPermission($account, 'access content');
//  }
}