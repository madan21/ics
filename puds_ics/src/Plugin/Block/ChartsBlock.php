<?php
/**
 * @file
 * Contains \Drupal\puds_ics\Plugin\Block\ChartBlock.
 */
namespace Drupal\puds_ics\Plugin\Block;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\puds_ics\Helper\Helper;

/**
 * Provides a 'charts' block.
 *
 * @Block(
 *   id = "chart_block",
 *   admin_label = @Translation("ICS charts block"),
 *   category = @Translation("PUDS custom blocks")
 * )
 */
class ChartsBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return  [
      '#theme' => 'ics_block',
      '#products' => Helper::getProducts()
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }
}