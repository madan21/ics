<?php

/**
 * @file
 * Contains \Drupal\puds_ics\Form\TermDeleteForm.
 */

namespace Drupal\puds_ics\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\puds_ics\Helper\Helper;

/**
 * Provides a form for deleting a content_entity_example entity.
 *
 * @ingroup dictionary
 */
class TermDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete entity %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the contact list.
   */
  public function getCancelUrl() {
    return new Url('entity.ics_term.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //TODO upload service
    $entity = $this->getEntity();
    $product_id = $entity->get('productid')->getValue()[0]['target_id'];
    $year = $entity->get('year')->getValue()[0]['value'];
    $week = $entity->get('week')->getValue()[0]['value'];
    $product = Helper::getProductById($product_id);
    $directory = 'sites/default/files/cache_ics_images/';
    //replace polish character and space from shortname as freshmail dosen't support
    $nonUnicode = preg_replace('/[^[:print:]\r\n]/','' , $product['shortname']);
    $file= preg_replace('/\s+/', '', $nonUnicode).'_'.$year.'_'.$week.'.png';
    if(file_exists($directory.$file)){
      unlink($directory.$file);
    }
    $entity = $this->getEntity();
    $entity->delete();
    $this->logger('puds_ics')->notice('deleted %title.',
      array(
        '%title' => $this->entity->label(),
      ));
    $form_state->setRedirect('entity.ics_term.collection');
  }

}
