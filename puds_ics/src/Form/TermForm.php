<?php
/**
 * @file
 * Contains \Drupal\puds_ics\Form\TermForm.
 */

namespace Drupal\puds_ics\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\puds_ics\Helper\Helper;

/**
 * Form controller for the content_entity_example entity edit forms.
 *
 * @ingroup content_entity_example
 */
class TermForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\dictionary\Entity\Term */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;
    $form['productid'] = [
      '#title' => 'Produkt',
      '#type' => 'select',
      '#default_value' => $entity->productid->target_id,
      '#options' => Helper::getProducts(),
      '#required' => true
    ];
    $form['year'] = [
      '#title' => 'Rok',
      '#type' => 'select',
      '#default_value' => $entity->year->value,
      '#options' => array_combine($r = range(2006, date('Y')+1), $r),
      '#required' => true
    ];
    $form['week'] = [
      '#title' => 'Tydzień',
      '#type' => 'select',
      '#default_value' => $entity->week->value,
      '#options' => array_combine($r = range(1, 53), $r),
      '#required' => true
    ];
    $form['minprize'] = [
      '#title' => 'Cena minimalna',
      '#type' => 'textfield',
      '#default_value' => $entity->minprize->value,
      '#required' => true
    ];
    $form['maxprize'] = [
      '#title' => 'Cena maksymalne',
      '#type' => 'textfield',
      '#default_value' => $entity->maxprize->value,
      '#required' => true
    ];
    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return \Drupal\Core\Entity\ContentEntityInterface|\Drupal\Core\Entity\ContentEntityTypeInterface|void
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if(!is_numeric($values['maxprize']) || $values['maxprize']<=0) {
      $form_state->setErrorByName('maxprize', 'Podana wartość musi być liczbą całkowitą większą od 0');
    }
    if(!is_numeric($values['minprize']) || $values['minprize']<=0) {
      $form_state->setErrorByName('minprize', 'Podana wartość musi być liczbą całkowitą większą od 0');
    }
    if($values['minprize'] > $values['maxprize']) {
      $form_state->setErrorByName('maxprize', 'Cena minimalna powinna być wyższa od maksymalnej');
    }
    if(!in_array($values['week'], range(1, 53))) {
      $form_state->setErrorByName('week', 'Błędna wartość');
    }
    if(!in_array($values['year'], range(2006, date('Y')+1))) {
      $form_state->setErrorByName('year', 'Błędna wartość');
    }
    if(!$form_state->getErrors()
      && (
        $values['form_id'] == 'ics_term_add_form' //add
        || ( //OR
          $form['week']['#default_value'] != $values['week'] //edit and values changed
          || $form['year']['#default_value'] != $values['year']
          || $form['productid']['#default_value'] != $values['productid']
        )
      )
    ) {
      //validate year/week/productid unique
      $connection = \Drupal::service('database');
      /** @var Select $query */
      $query = $connection->select('ics_history', 'i')->fields('i')->where('`productid`=:productid and `year`=:year and `week`=:week', [
        ':productid' => $values['productid'],
        ':year' => $values['year'],
        ':week' => $values['week'],
      ]);
      if($query->countQuery()->execute()->fetchField()) {
        $form_state->setErrorByName('productid', 'Cena dla podanego tygodnia została już wprowadzona');
      }
    }
    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    //TODO create upload service
    $values = $form_state->getValues();
    $product = Helper::getProductById($values['productid']);
    $directory = 'sites/default/files/cache_ics_images/';
    //replace polish character and space from shortname as freshmail dosen't support
    $nonUnicode = preg_replace('/[^[:print:]\r\n]/','' , $product['shortname']);
    $file= preg_replace('/\s+/', '', $nonUnicode).'_'.$values['year'].'_'.$values['week'].'.png';
    if(file_exists($directory.$file)){
      unlink($directory.$file);
    }
    $form_state->setRedirect('entity.ics_term.collection');
    $entity = $this->getEntity();
    $entity->save();
  }
}
