<?php
namespace Drupal\puds_ics\Helper;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Select;

class Helper {
  CONST ICS_START_WEEK = 38;
  CONST ICS_START_YEAR = 2006;

  CONST ICS_PRODUCT_BLACHA = 1;
  CONST ICS_PRODUCT_HEB = 2;
  CONST ICS_PRODUCT_PRET = 3;

  public static function getWeekYear($weekMov=0, $yearMov=0) {
    $date = new \DateTime();
    if($weekMov) {
      $date->modify(($weekMov > 0 ? '+' : '').$weekMov .' week');
    }
    if($yearMov) {
      $date->modify(($yearMov > 0 ? '+' : '').$yearMov .' year');
    }
    return [$date->format('W'), $date->format('Y')];
  }

  public static function getProducts() {
    /** @var Connection $connections */
    $connection = \Drupal::service('database');
    /** @var Select $query */
    $query = $connection->select('ics_products', 'i')->fields('i');
    $result = [];
    foreach($query->execute()->fetchAll() as $product) {
      $result[$product->id] = $product->name;
    }
    return $result;
  }

  /**
   * Return product details by id
   * @param $id
   * @return mixed
   */
  public static function getProductById($id)
  {/** @var Connection $connections */
    $connection = \Drupal::service('database');
    /** @var Select $query */
    $query = $connection->select('ics_products', 'i')->fields('i')->where('`id`=:id',[
      ':id' => $id,
    ]);
    $result = $query->execute()->fetchAssoc();
    return $result;

  }
}