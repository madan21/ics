(function (global, $, undefined) {

    function getDataFor(element) {
        var id = $(element).data('id');
        var data = [];

        var url = '/ics/get/' + id + '?showAll=1';
        var request = $.get(url);
        // Callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            if (response.status == 'success') {
                var maximum = {
                    'area': false,
                    'key': 'maksimum',
                    'color': '#077cff',
                    'values': []
                };
                var minimum = {
                    'area': false,
                    'key': 'minimum',
                    'color': '#cacbd0',
                    'values': []
                };
                $.each(response.data, function (index, value) {
                    maximum.values.push({'x': moment().year(value.y).week(value.w).valueOf(), 'y': parseFloat(value.max)});
                    minimum.values.push({'x': moment().year(value.y).week(value.w).valueOf(), 'y': parseFloat(value.min)});
                });
                data.push(maximum);
                data.push(minimum);
                //hide tooltip
                maximum.disableTooltip = true;
                minimum.disableTooltip = true;

                var len = data[0].values.length;
                var chart = nv.models.lineWithFocusChart();

                chart.xAxis.tickFormat(function (d) {
                    return moment(d).format('Y-ww');
                }).axisLabel("Czas");
                chart.x2Axis.tickFormat(function (d) {
                    return moment(d).format('Y-ww');
                });
                chart.focusEnable(false);
                chart.focusHeight(false);
                chart.showXAxis(false);
                chart.showYAxis(false)
                chart.showLegend(false);

                chart.y2Axis.tickFormat(d3.format(',.2f'));
                var svg = d3.select('.chart[data-id="' + id + '"] svg');

                //disable tooltip using css
                svg.style('pointer-events', 'none'); //remove mouse event to show tooltip

                svg.datum(data)
                  .transition().duration(500)
                  .call(chart);
                nv.utils.windowResize(chart.update);
                var baseSVG = element.getElementsByTagName('svg')[0];
                var canvas = prepareCanvas(baseSVG, 2);
                addBackgroundToCanvas(canvas);
                var promise = new Promise((resolve, reject) => {
                    resolve(prepareImageData(element))
                });
                promise.then((result) => {
                    var drawingFn = isIE11() ? drawOnCanvasForIE11 : drawOnCanvas;
                    drawingFn(canvas, result, function () {
                        var imgDimensions = getPngImageDimensions(canvas);
                        var img = canvas.toDataURL('image/png',1.0);
                        var form = $('<form></form>');
                        var formData = new FormData(form[0]);
                        var contentType = 'image/png';
                        var splitString = img.split(',');
                        var b64Data = splitString[1];
                        var blob = b64toBlob(b64Data, contentType);
                        var shortname = response.shortname.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
                        formData.set('documentFile', blob,shortname +'_'+response.year+'_'+response.week+ '.png');
                        $.ajax({
                            url: '/ics/upload_image',
                            type: 'POST',
                            data: formData,
                            cache: false,
                            processData: false,
                            dataType: 'json',
                            contentType: false,
                            success: function (response) {
                                console.log(response.message);
                            }
                        })
                    });
                })

            }
        });
    }

    function prepareCanvas(baseSVG, scale) {
        var svgDimensions = baseSVG.getBoundingClientRect();
        var canvas = document.createElement('canvas');
        canvas.width = svgDimensions.width;
        canvas.height = svgDimensions.height;
        return canvas;
    }

    function prepareImageData(baseSVG) {
        return svgAsDataUri(baseSVG.getElementsByTagName('svg')[0], {backgroundColor: "#FFFFFF"});
    }

    function addBackgroundToCanvas(canvas) {
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    function isIE11() {
        return navigator.userAgent.includes('MSIE') || navigator.userAgent.includes('Trident/');
    }

    function getPngImageDimensions(canvas) {
        var xMargin = 4;
        var yMargin = 10;
        var pixelToMillimeter = 0.264583333;
        var a4SheetWidthMillimeters = 297 - xMargin * 2;
        var width = canvas.width * pixelToMillimeter;
        var height = canvas.height * pixelToMillimeter;

        if (width > a4SheetWidthMillimeters) {
            height = a4SheetWidthMillimeters / width * height;
            width = a4SheetWidthMillimeters;
        }

        return {x: xMargin, y: yMargin, width: width, height: height};
    }

    function drawOnCanvas(canvas, imageData, exportProcessFn) {
        var img = new Image();
        var ctx = canvas.getContext('2d');
        img.src = imageData;
        img.onload = function () {
            ctx.drawImage(img, 0, 0, canvas.width , canvas.height);
            exportProcessFn()
        };
    }

    function drawOnCanvasForIE11(canvas, baseSVG, exportProcessFn) {
        canvg(canvas, (new XMLSerializer()).serializeToString(baseSVG), {
            ignoreDimensions: true,
            ignoreClear: true,
            ignoreMouse: true,
            ignoreAnimation: true,
            scaleWidth: canvas.width,
            scaleHeight: canvas.height
        });

        exportProcessFn();
    }

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
    }

    $('.chart').each(function (i, element) {
        getDataFor(element);
    });

}(window, window.jQuery));