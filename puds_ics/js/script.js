(function (global, $, undefined) {
  //var baseSVG = document.getElementById('chart').getElementsByTagName('svg')[0];
  var pdfData = {};
  //Default length of graph shown
  var brushStartlength = 52;
  var brushEndlength = 1;

  //calender MinDate and maxDate default
  jQuery.datetimepicker.setLocale('pl');
  var calenderMinDate = moment().year('2016').week('38').format("YYYY-MM-DD");
  var calenderMaxDate = moment().format("YYYY-MM-DD");

  function getDataFor(element) {
    var id = $(element).data('id');
    var block = $(element).data('block');
    var data = [];
    // var startdate = '';
    // var enddate = '';
    var url = '/ics/get/' + id;
    // if ($('[name="startdate"]').val()) {
    //   startdate = $('[name="startdate"]').val();
    //   url += '/' + startdate;
    // }
    // if ($('[name="enddate"]').val()) {
    //   enddate = $('[name="enddate"]').val();
    //   url += '/' + enddate;
    // }
    if(block && $('[data-block="1"][data-done="1"]').length){
      //copy old to new
      var svgCopy = $('[data-id="'+ id +'"][data-done="1"]').clone();
      $(element).html(svgCopy);
      return
    }
    var request = $.get(url);
    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR) {
      if (response.status == 'success') {
        //change attribute after success to check later block already render/
        $('[data-id="'+ id +'"]').attr('data-done', 1);
        $('.week span').text(response.week);
        var product_info = $(element).find('.product_info');
        product_info.find('.name').text(response.name);
        product_info.find('.min span').text(response.min);
        product_info.find('.max span').text(response.max);
        if (response.data_previous_week) {
          product_info.find('.previous_week span').text(response.data_previous_week.w);
          product_info.find('.previous_week_min span').text(response.data_previous_week.min);
          product_info.find('.previous_week_max span').text(response.data_previous_week.max);
          product_info.find('.min_change_week span:first').text(response.data_previous_week.change_min);
          product_info.find('.min_change_week span:last').text(response.data_previous_week.percent_min);
          product_info.find('.max_change_week span:first').text(response.data_previous_week.change_max);
          product_info.find('.max_change_week span:last').text(response.data_previous_week.percent_max);
          product_info.find('.previous_year span:first').text(response.data_previous_year.y);
          product_info.find('.previous_year span:last').text(response.data_previous_year.w);
          product_info.find('.previous_year_min span').text(response.data_previous_year.min);
          product_info.find('.previous_year_max span').text(response.data_previous_year.max);
          product_info.find('.min_change_year span:first').text(response.data_previous_year.change_min);
          product_info.find('.min_change_year span:last').text(response.data_previous_year.percent_min);
          product_info.find('.max_change_year span:first').text(response.data_previous_year.change_max);
          product_info.find('.max_change_year span:last').text(response.data_previous_year.percent_max);
        }
        var maximum = {
          'area': false,
          'key': 'maksimum',
          'color': '#077cff',
          'values': []
        };
        var minimum = {
          'area': false,
          'key': 'minimum',
          'color': '#cacbd0',
          'values': []
        };
        $.each(response.data, function (index, value) {
          maximum.values.push({'x': moment().year(value.y).week(value.w).valueOf(), 'y': parseFloat(value.max)});
          minimum.values.push({'x': moment().year(value.y).week(value.w).valueOf(), 'y': parseFloat(value.min)});
        });
        data.push(maximum);
        data.push(minimum);
        if(!block)
        {
          pdfData = {
            'name': response.name,
            'shortname': response.shortname,
            'minPrice': response.min,
            'maxPrice': response.max,
            'week': response.week,
            'year': response.year,
            'minPricePWeek': response.data_previous_week.min,
            'maxPricePWeek': response.data_previous_week.max,
            'minPercentagePWeek': response.data_previous_week.percent_min,
            'maxPercentagePWeek': response.data_previous_week.percent_max,
            'minChangePWeek': response.data_previous_week.change_min,
            'maxChangePWeek': response.data_previous_week.change_max,
            'minPricePYear': response.data_previous_year.min,
            'maxPricePYear': response.data_previous_year.max,
            'minPercentagePYear': response.data_previous_year.percent_min,
            'maxPercentagePYear': response.data_previous_year.percent_max,
            'minChangePYear': response.data_previous_year.change_min,
            'maxChangePYear': response.data_previous_year.change_max,
          };
        }else{
          //hide tooltip in block
          maximum.disableTooltip = true;
          minimum.disableTooltip = true;
        }

        var len = data[0].values.length;
        var chart = nv.models.lineWithFocusChart();

        chart.xAxis.tickFormat(function (d) {
          return moment(d).format('Y-ww');
        }).axisLabel("Czas");
        chart.x2Axis.tickFormat(function (d) {
          return moment(d).format('Y-ww');
        });
        if (!block) {
          chart.brushExtent([new Date(data[0].values[len - 52].x), new Date(data[0].values[len - 1].x)]);
        } else {
            chart.focusEnable(false);
            chart.focusHeight(false);
            chart.showXAxis(false);
            chart.showYAxis(false)
            chart.showLegend(false);
        }
        chart.useInteractiveGuideline(true);
        chart.y2Axis.tickFormat(d3.format(',.2f'));
        var svg = d3.select('.chart[data-id="' + id + '"] svg');
        //Add margin for block view page
        if (block) {
          svg.attr('style', 'margin-left:-45px');
          svg.style('pointer-events', 'none'); //remove mouse event to show tooltip
        }
         svg.datum(data)
          .transition().duration(500)
          .call(chart);
        nv.utils.windowResize(chart.update);
        if(!block)
        {
              //Calender start and end date from graph
              // calenderMinDate = moment(data[0].values[0].x).format('YYYY-MM-DD');
              // calenderMaxDate = moment(data[0].values[len -1 ].x).format('YYYY-MM-DD');
              //setStartDate();
              //setEndDate();

              $('#chart-brush-x').submit(function (event) {
                  event.preventDefault();
                  if ($('#startdate').val() && $('#enddate').val()) {
                      if (moment($('#startdate').val()) < moment($('#enddate').val())) {
                          var startDate = moment($('#startdate').val());
                          var endDate = moment($('#enddate').val());
                          var chartEndDate = moment(data[0].values[len - 1].x);
                          if(endDate > chartEndDate){
                              brushEndlength = 1;
                              var diffWeek = chartEndDate.diff(startDate,'weeks');
                              brushStartlength = diffWeek?brushEndlength + diffWeek:brushEndlength + 1;
                          }else{
                              var brushEndWeek = chartEndDate.diff(endDate, 'weeks');
                              brushEndlength += brushEndWeek;
                              var diffWeek = endDate.diff(startDate,'weeks');
                              brushStartlength = diffWeek?brushEndlength + diffWeek:brushEndlength + 1
                          }
                          chart.brushExtent([new Date(data[0].values[len - brushStartlength].x), new Date(data[0].values[len - brushEndlength].x)]);
                          svg.datum(data)
                            .transition().duration(500)
                            .call(chart);
                          nv.utils.windowResize(chart.update);
                      } else {
                          $('.date-error').text('Data początkowa jest późniejsza niż data końcowa.');
                      }
                  }
              });

              $('[name="startdate"], [name="enddate"]').on('change', function () {
                  $('.date-error').text('');
              });

              document.getElementById('download-pdf').addEventListener('click', function (e) {
                  e.preventDefault();
                  exportToPdf(element);
              });
          }
                    }
            });
    }

  function exportToPdf(element) {
    var baseSVG = element.getElementsByTagName('svg')[0];
    var canvas = prepareCanvas(baseSVG, 8);
    addBackgroundToCanvas(canvas);
    $(".nv-focusWrap").removeAttr("style").attr("display", "none");
    $(".nv-legendWrap").attr("display", "none");
    var xMaxVal = d3.select('.chart svg').selectAll('.nv-axisMax-x text');
    xMaxVal.attr('style','text-anchor: end');
    var promise = new Promise((resolve, reject) => {
      resolve(prepareImageData(element))
    });
    promise.then((result) => {
      var drawingFn = isIE11() ? drawOnCanvasForIE11 : drawOnCanvas;
      drawingFn(canvas, result, function () {
        $(".nv-focusWrap").removeAttr("display").attr("style", "display: initial");
        $(".nv-legendWrap").removeAttr("display");
        xMaxVal.attr('style','text-anchor: middle');
        exportPdf(canvas);
      });
    })
  }

  function drawOnCanvas(canvas, imageData, exportProcessFn) {
    var img = new Image();
    var ctx = canvas.getContext('2d');
    img.src = imageData;
    img.onload = function () {
            ctx.drawImage(img, 100, 100, canvas.width , canvas.height );
      exportProcessFn()
    };
  }

  function drawOnCanvasForIE11(canvas, baseSVG, exportProcessFn) {
    canvg(canvas, (new XMLSerializer()).serializeToString(baseSVG), {
      ignoreDimensions: true,
      ignoreClear: true,
      ignoreMouse: true,
      ignoreAnimation: true,
      scaleWidth: canvas.width,
      scaleHeight: canvas.height
    });

    exportProcessFn();
  }

  function prepareImageData(baseSVG) {
    return svgAsDataUri(baseSVG.getElementsByTagName('svg')[0], {backgroundColor: "#FFFFFF"});
  }

    function prepareCanvas(baseSVG, scale) {
        var svgDimensions = baseSVG.getBoundingClientRect();
        var canvas = document.createElement('canvas');
    canvas.width = svgDimensions.width * scale;
    canvas.height = svgDimensions.height * scale;
        return canvas;
    }

  function addBackgroundToCanvas(canvas) {
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
  }

  function isIE11() {
    return navigator.userAgent.includes('MSIE') || navigator.userAgent.includes('Trident/');
  }

  function exportPdf(canvas) {
    var imgDimensions;
    var doc = new jsPDF({
      orientation: 'L',
      unit: 'mm',
      format: 'a3'
    });
    //to support polish letter.
    doc.addFont("Arimo-Regular.ttf", "Arimo", "normal");
    doc.addFont("Arimo-Bold.ttf", "Arimo", "bold");
    doc.setFont("Arimo");
    doc.setFontType("normal");

    imgDimensions = getImageDimensions(canvas);
    doc.addImage(
      canvas.toDataURL('image/jpeg',1.0),
      'JPEG',
      imgDimensions.x,
      imgDimensions.y,
      imgDimensions.width,
      imgDimensions.height,
      '',
      'FAST'
    );

    doc.setFontType("bold");
    doc.text(20, 45, pdfData.name);
    doc.text(230, 45, 'Indeks cen stali PUDS');

    doc.setFontType("normal");
    doc.setFontSize(9);
    doc.text(20, 50, 'Cena min: ' + pdfData.minPrice + ' PLN', {});
    doc.text(20, 55, 'Cena max: ' + pdfData.maxPrice + ' PLN', {});

    doc.text(230, 50, "Tydzień: " + pdfData.week + "  Rok: " + pdfData.year, {lang: 'pl'});

    doc.text(80, 240, 'Poprzedni tydzień ( zmiana ) ', {});
    doc.text(155, 240, 'Poprzedni rok ( zmiana ) ', {});

    doc.text(20, 248, 'Cena minimalna: ', {});
    doc.text(20, 255, 'Cena maksymalna: ', {});

    doc.text(80, 248, pdfData.minPricePWeek + ' PLN ( ' + pdfData.minChangePWeek + ' PLN / ' + pdfData.minPercentagePWeek + ' % )', {});
    doc.text(80, 255, pdfData.maxPricePWeek + ' PLN ( ' + pdfData.maxChangePWeek + ' PLN / ' + pdfData.maxPercentagePWeek + ' % )', {});

    doc.text(155, 248, pdfData.minPricePYear + ' PLN ( ' + pdfData.minChangePYear + ' PLN / ' + pdfData.minPercentagePYear + ' % )', {});
    doc.text(155, 255, pdfData.maxPricePYear + ' PLN ( ' + pdfData.maxChangePYear + ' PLN / ' + pdfData.maxPercentagePYear + ' % )', {});

    var img = new Image;
    img.crossOrigin = "";
    img.src = "../modules/custom/puds_ics/images/logo.jpeg";
    img.onload = function () {
      doc.addImage(this, 10, 10);
      doc.setDisplayMode('original');
      doc.save('ics_'+ pdfData.shortname +'_'+ moment().format("YYYY-MM-DD")+'.pdf');
    };
  }

  function getImageDimensions(canvas) {
    var xMargin = 3;
    var yMargin = 50;
    var pixelToMillimeter = 0.264583333;
    var a4SheetWidthMillimeters = 297 - xMargin;
    var width = canvas.width * pixelToMillimeter;
    var height = canvas.height * pixelToMillimeter;
    if (width > a4SheetWidthMillimeters) {
      height = a4SheetWidthMillimeters / width * height;
      width = a4SheetWidthMillimeters;
    }
    return {x: xMargin, y: yMargin, width: width, height: height};
  }

  $('.chart').each(function (i, element) {
    getDataFor(element);
  });

  function setStartDate() {
    $('#startdate').datetimepicker({
      //yearOffset:222,
      lang: 'pl',
      timepicker: false,
      format: 'Y-m-d',
      formatDate: 'Y-m-d',
      minDate: calenderMinDate,
      maxDate: calenderMaxDate
    });
  }

  function setEndDate() {
    $('#enddate').datetimepicker({
      //yearOffset:222,
      lang: 'pl',
      timepicker: false,
      format: 'Y-m-d',
      formatDate: 'Y-m-d',
      minDate: calenderMinDate,
      maxDate: calenderMaxDate
    });
  }

  //load chart twice in block view
  $(document).ajaxComplete(function(event, xhr, settings){
    if(settings.type == "POST" && typeof settings.data != 'undefined' && settings.data.search('weekly') > 0) {
      //check only for new render div with attribute data-done
      $('.chart[data-done="0"]').each(function (i, element) {
        getDataFor(element);
      });
    }
  });

  setStartDate();
  setEndDate();

}(window, window.jQuery));